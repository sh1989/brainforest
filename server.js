const express = require('express');

const server = express();
const port = 3000;

server.use(express.static('.'));
server.use(express.static('notes'));
server.listen(port, () => {
  console.log(`Development server running on :${port}`);
});
