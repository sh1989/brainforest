Audiences can't maintain focus indefinitely. To keep sessions engaging the presenter needs to employ a few techniques:
* Introduce rest points
* Vary the tempo
* Have a clear beginning, middle and end
* Utilise different senses

> “Do not confuse motion and progress. A rocking horse keeps moving but does not make any progress.” - Alfred A. Montapert

Varying the intensity in this way creates a richer experience