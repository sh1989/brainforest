Implement the option as quickly as possible and learn how to speed up the process.

For every decision, there's a cost of deferral - this grows over time. There's a corresponding cost of deciding, which decreases as more information is revealed (which is why [[Gathering information reveals more options]]). As long as the cost of deciding is above the cost of deferral, it may not be the **most** responsible moment to [[Exercise an option to realise its value]]. The intersection point is the [[Last responsible moment]]  and this is where commitment should be made.

Pushing back the last responsible moment as an options management technique maximises the value realised from the option.