The rules of lean options are:
* [[Options have value]]
* [[Options expire]]
* [[Never commit early unless you know why]]

In the context of [[Agile]] development, [[An Option is freedom of choice]] - rather than sticking to a definite plan of action, which builds risk up until the point of delivery, options act as risk-reducers.

In a more general sense, [[Options reduce risk for an uncertain future]]
