## Record
[[How to take notes from literature]]

## Elaborate
[[Take thoughtful notes]]
[[An evergreen note is a unit of knowledge]]
[[Writing is thinking]]

## Integrate
[[Take smart notes]]
[[Prefer organic structure to hierarchical structure]]
