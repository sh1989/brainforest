There is a point in time at which an option loses its value - it is no longer possible to [[Exercise an option to realise its value]]. As part of managing a portfolio of options, the conditions at which an option expires should be known.

In the traditional financial sense options expire by a certain date. This can be true in the real world, but there are plenty of other reasons why we may expire an option. This is a window of opportunity.

Until the point of expiry:
* [[Look for new options]]
* [[Attempt to push back the time to make a decision]]