Beware of context, what's obvious to one person may not be obvious to another. We have diverse experiences, diverse backgrounds and diverse personalities. The way that we learn, and what we learn, differs as a result.

Put the [[Journey before destination]] by understanding where people are on their journey and helping them to take the next steps - [[Learn along the way]]