Mocks create a false narrative when they are used to bypass actual behaviour. This can result in test code that doesn't actually follow how it's used in reality. We should aim to simplify the complexity of the code that the mocks are masking.

Although [[Fast feedback improves quality]] is a good reason for introducing mocks in the code, especially at boundaries, overly-mocked code slows down feedback because of the alternate reality. You may still end up with tests that are running quickly, but if they're no longer representative then the feedback has little utility.

Mocks which are used internally at levels of indirection, if produced without abstraction can further damage the maintainability of production and test code, which is why [[Indirection without abstraction is waste]]

Fixed by [[Test behaviour not implementation]]

## References
[Ashley Frieze, How Mocks Ruin Everything](https://codingcraftsman.wordpress.com/2017/01/31/how-mocks-ruin-everything/)
[[Beyond Mock Objetcs]]