A backlink is a bi-modal connection between two nodes, preserving contextual information and directionality of the association.

When Article A links to Article B, Article B also knows about the incoming link and why the link is made.

Backlinks are an essential part of the digital garden topology. [[Link evergreen notes by contextual association]] and also track which [[Evergreen notes]] link to the current context.

[[Optimistically creating note stubs will surface new concepts]] when writing new notes, because as [[Writing is thinking]], creating a link to a new contextual note that doesn't exist yet represents a [[Save point]] for your mind. You may not come back to it immediately, or upon successive revisions, but [[The graph of notes identifies unrealised associations]] by creating artificial nodes for these stubbed notes that can then be expanded upon. Likewise, [[Implicit backlinks identify unrealised associations]] in cases where the author did not link to the stubbed concept directly from other notes. Tooling can then turn these into true links.