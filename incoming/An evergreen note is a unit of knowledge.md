The notion of an evergreen note is to represent a concept, an idea, an insight, in an "atomic" way. In that sense, they can be considered a core building block of a knowledge management system such as a second brain.

Traditional note-taking anchors the notes to the resource. This makes recall different, to get to an insight, one must remember the source material in which the insight was gained. Evergreen notes invert this idea, making the insight itself the first-class citizen. As an insight independent of the resource, the note can evolve and accumulate over time.

The [[Curation of ideas broadens horizons and helps draw patterns]] - an act of [[Deliberate practice]] in which existing evergreen notes are reviewed and linked to other notes. This emergent property facilitates the accumulation of knowledge.

Evergreen notes have no lower or upper limit, however it is occasionally helpful to split out notes into smaller and related concepts. This may leave the outer note as a structure note, or a [[Map of Content]], although it is better to [[Prefer organic structure to hierarchical structure]].