As [[Remote working is here to stay]], pre-pandemic notions that meetings are primarily co-located with some remote participants cannot hold.

Remote workers cannot be made to feel like second-class citizens, who are felt excluded by their inability to play off the inter-personal interactions in the meeting room, or through poor-quality equipment find they aren't able to effectively contribute.

This requires a culture change in an organisation and needs to be supported through:
* Infrastructure investments in teleconferencing equipment
* Diversity, inclusion and unconscious bias training
* Better, structured meetings