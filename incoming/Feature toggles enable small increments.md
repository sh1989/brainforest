Sometimes we can't decompose a feature when presenting it to a user. But, that doesn't mean we have to deliver it all in one go, or keep it off the mainline branch during its development.

Consider scaffolding while doing home repair work - a structure is put in place that enables the development work to be performed safely with minimal impact to the functioning of the building elsewhere.

Feature toggles are a mechanism to develop features on the main branch in small increments, without exposing the incomplete feature to end users. As [[Small increments act as save points]] - this development practice avoids huge merge conflicts incurred from the pull request model - [[Pull Requests encourage rework]] and therefore [[Pull Requests can hinder Continuous Integration]]