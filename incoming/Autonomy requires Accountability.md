> Autonomy without accountability is called a vacation
(What's the source for this)?

How do we distinguish between accountability and blame?
What does being accountable mean in this sense?

How does accountability empower us when working in an [[Agile]] way?

From: https://www.scrum.org/resources/blog/balancing-autonomy-accountability

Scrum is a transparent and empirical process.
If we see the act of project management in the sense of allocating work and tracking progress to completion, then accountability can be very easily seen as 'blame'. Whereas it should be one of people management, that of growing people.

We are moving from being able to pretend we can answer "when will this be done" upfront by locking things down and working that plan, to one where we forecast continually.

> The teams weren't being held to account to deliver 'done' increments of value every iteration

From: https://itrevolution.com/accountability-leaders-are-accountable-too/

Accountability is traditionally: "who can I hold accountable"? This is very punitive. But if you were accountable, you were obligated to render account - which is different. If you break a dish while cooking, you clean it up. Accountability is not something to be afraid of, but to be embraced. Nor is it something to be imposed, but shared.

Ask yourself: what can I be doing differently? If I want the [[Culture]] to be different, I should model that.

From: https://www.scrum.org/resources/blog/scrum-accountability

Scrum emphasizes accountability to establish checks and balances, which enable empiricism and self-organisation.
Pulled rather than pushed (upon teams) and supporting Scrum Values (for psychological safety)

The Scrum Guide uses responsibility and accountability synonymously (2020 must have picked one)

See (Amateurs vs Professionals)[https://fs.blog/2017/08/amateurs-professionals/)

>     “Amateurs make decisions in committees, so there is no one person responsible if things go wrong. Professionals make decisions as individuals and accept responsibility.”
    “Amateurs blame others. Professionals accept responsibility.”
	
I fundamentally disagree with this - the difference is purely the financial aspect.


## Next Steps
(Watch: https://www.youtube.com/watch?v=yeA4CBInqKo&list=PLE9763518A2765373)