The medium in which a message is expressed also shapes that expression. There is a difference between how information is represented internally as a thought, versus how that information is expressed, whether verbally (through speech patterns, intonation), or in written form.

To some degree, the capabilities offered in a particular medium will then influence the dissemination of information.

> We see the world through a rear-view mirror. We march backwards into the future
> Marshall McLuhan, The Medium is the Message  

^rear-view-mirror