A type of [[Higher-Order Note]] to provide organisational structure for the purposes of navigation and retrieval.

## Some Other Maps of Content
* [[Agile]]
* [[Coaching]]
* [[Culture]]
* [[Role-Playing Games]]
* [[Software Development]]
* [[Test-Driven Development]]
