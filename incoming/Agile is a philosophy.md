Criticisms of agile tend to fall into descriptions of [[Is Agile a Cult|cults or religions]], based on what I see to be a misapplication of the ideas. This is because I understand agility to be a philosophy, or perhaps an ideology.

[[Agile is a perspective not a process]], although many look to some common 'implementations' of Agile methodologies such as [[Scrum]] or [[SAFE]] for an off-the-shelf solution. What makes this technique unsuccessful is when:
* There is a rigid implementation of process, without the ability or agency to change that process. This is often waterfall in disguise, or [[Agile in Name Only]]
* There is pressure to stick to the plan, rather than [[Learn Along the Way]].
* There is little goal forming other than a vague sense of "complete the work" ([[Goals tell us the ends not the means]]) and therefore little to no [[Experiment towards a goal|experimentation and discovery]]
* Fixed deadlines with fixed scope, which do not promote the ability for a team to [[Work at a sustainable pace]]
* Timeboxes are planned to [[Optimise for flow, not resource capacity|capacity, not flow]]
* [[User Stories]] are treat as hard requirements and not [[A User Story is a placeholder for a conversation|tools for a conversation]]

This can lead to thinking that "being Agile" is the goal, [[Agile is not the goal|but it is not]]. Actually, when considering that [[Agility is a business capability]], Agile processes are better seen as means of competing in a complex and uncertain marketplace, through incorporating learning, experimentation and [[Fast feedback improves quality|fast feedback]]