The art of discovering bugs in software is in identifying the misunderstandings that could be theoretically present, such as:
* Between "business" and "the development team"
* Between a "developer" and a "tester"
* Between the organisation and its users
* Between the desired effects and what was actually implemented
* Between a developer and their operating environment (such as the web browser, the language runtime, etc)

Additionally, "shifting testing left" is about trying to identify these misunderstandings, and **clarify** them, as early as possible. The later in the process they're discovered, the more expensive it is to fix them. Many "bugs" can be fixed purely through conversations at the right moment in time - [[The quality of conversations drives the quality of the product]]