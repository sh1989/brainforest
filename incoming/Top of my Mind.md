I want to learn more about [[Structured Content]], particularly after thinking through the implications of [[Where does Markdown fall short]]. I've taken an interest in [[Web Components]] recently and therefore the [[Block Protocol]] seems interesting. I need to experiment! Why?
* In the context of [[Software Development]] I want to understand how to better facilitate [[Living documentation]]
