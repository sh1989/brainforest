Maps are imperfect shorthand representations. By abstracting detail they lose precision, but become more accessible through their reduced cognitive load.

The original reference to this comes from Alfred Korzybski
> A map is not the territory it represents, but, if correct, it has a similar structure to the territory, which accounts for its usefulness.
