It is possible to use the [[Cynefin]] framework as a sensemaking tool:

* Emergent and non-deterministic work changes the space it operates in by discovering unknown unknowns. Variability is a tool for learning
* Deterministic work wants to reduce variability and increase standardisation