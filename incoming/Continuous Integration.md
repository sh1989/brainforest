It is common for developers to treat Continuous Integration as "a build server", but it is more than that. [[Continuous integration optimises team flow]] by encouraging them team to regularly rendezvous via the main development branch.

> My general rule of thumb is that every developer should commit to the repository every day. In practice it's often useful if developers commit more frequently than that.
> [[Martin Fowler]]

Therefore, [[Pull Requests can hinder Continuous Integration]] by destabilising the overall team flow. Team co-ordination several times a day is a form of [[Fast feedback improves quality]], as [[The quality of conversations drives the quality of the product]]. When conflicts arise, they're fixed on a small turnaround through team communication.

Continuous integration as a development practice therefore encourages working in small increments that are easy to integrate ([[Small increments act as save points]]), or revert. Implementation techniques arise from this, such as:
* [[Feature toggles enable small increments]]