To support people in making autonomous decisions about how to achieve a desired outcome (and to surface impediments or additional context that may cause difficulty in achieving this outcome), it is necessary to make sure the intent behind the outcome is well-understood.

When clashes arrive, it could be because the intent was not suitably clarified.

In fact, this itself is a testing process, as [[Testing is clarifying misunderstandings]]

Source: https://lizkeogh.com/2021/06/11/5-rules-of-coaching/