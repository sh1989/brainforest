It can sometimes feel that Agile is a Cult. [[Question]] I wonder, what's the true motivation behind provocative statements like this - what's _not being said_? And even if it is true, what can be learned from that?

## Properties Often-Cited As Cult/Religion-Like
* There's no evidence that Agile methodologies lead to better performance or business value - and that actually all that's needed psychologically is a willingness for process improvements
* The certification mill, combined with one of the certification bodies requiring yearly subscriptions, is off-putting. "pay to play" and status symbols but with little demonstrable value of practical learning or experience - [[Agile Industrial Complex]]
* Agile is often seen as an proselytizing movement with evangelists
* A dogmatic feel to running ceremonies
* The way that we quote the manifesto, very often, reminds me of the way that priests quote chapter and verse.
* Faith in "the process" to produce results, unspecified emergent effects hitherto unknown
* It's a superstition in that correlation is not causation but is positively reinforced
* If there are problems, it is because the process is working and making these visible, or that the team aren't doing agile correctly or that there's not buy in from a resistant/hesitant group

## Where does this come from?
I have observed a somewhat cynical sentiment arising from poor processes that cannot lead to working with agility and do not deliver on the outcomes Agile claims to possess. For many my age, Scrum *is* the way of doing things, more than waterfall. "Meet the new boss, same as the old boss." ([[Agile in Name Only]])

In large organisations, where agile processes are tacked onto existing governance structures (ITIL), it can be difficult for the team to actually work in a way that will allow for continuous improvements. This is where morale starts to sink and people think that agile is just a bunch of ceremonies, people talking but not doing, and without "business buy-in" there's little point. User stories are just new names for requirements.

## What differs a cult from a religion?
* 'Extreme'
* Financial aspect
* Control, lack of questioning

## If it is a religion, how does that help?
Religions, like all communities, have a foundational set of values. Religious service places structure and regularity to teaching these values and applying them to current context. 

## Well, Actually
- Scrum describes itself as an empirical framework and its events, accountabilities and artefacts are designed to promote empirical thinking.
- What's special about the manifesto is it hit the zeitgeist, but that's about it. It's actually deliberately vague and avoids being seen as commandments by acknowledging there's value on both sides of "X over Y"
- [[Agile is a philosophy]]

## A New Radicalism?
* [[Agile is not the goal]], [[Agility is a business capability]]
* [[Learn along the way]]
* [[Fast feedback improves quality]]
* [[User stories are told, not written]]
* [[We can only be certain about now]]
* [[Deliver sooner, not faster]]
* [[Goals tell us the ends not the means]]
* [[Experiment towards a goal]]
* [[Estimates aren't quotes]]
* [[Autonomy requires accountability]]

## Reading
- http://www.agilekiwi.com/other/agile/faith-doubt-and-evidence-agile-as-religion-versus-agile-as-social-science/
- https://jhall.io/archive/2021/04/24/agile-isnt-a-religion/
- http://steve-yegge.blogspot.com/2006/10/egomania-itself.html?m=1
- http://steve-yegge.blogspot.com/2006/09/good-agile-bad-agile_27.html?m=1
- https://georgobermayr.com/articles/agile-is-not-a-religion
- https://pikilon.medium.com/agile-the-right-way-religion-b3b6182cce75
- https://agilegnostic.wordpress.com/2015/06/19/i-think-agile-as-a-religion/
- [[Agile Uncertified - Philosophy over Rituals]]