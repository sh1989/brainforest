From the Agile Manifesto:
> Simplicity--the art of maximizing the amount of work not done--is essential.

The act of prioritization is a manifestation of choosing what to work on next, to maximize value. Prioritising a piece of work for delivery is exercising the option - to seek to extract value from the item.

Product Owners manage a portfolio of options on a backlog. [[User Stories Are Options, Not Futures]] because there is no guarantee that work on the backlog will ever get prioritized. However by doing detailed analysis on a user story before it's prioritized for delivery, the option is exercised early.