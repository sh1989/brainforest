Working with agility is an outcomes-focused collaborative approach to developing product or services, which empowers a team to deliver value frequently and sustainably through self-improving mechanisms.

[[Differences Between Agile and Taylorism]]

By establishing clear goals in terms of the valuable outcomes that we want to achieve ([[Goals tell us the ends not the means]]), a team can [[Experiment towards a goal]] and [[Learn along the way]] to frequently evaluate if they're building the right thing in the right way.

[[Fast feedback improves quality]], so agility requires teams to optimise their workflows and dynamics for delivering value. Considering that [[Small increments act as save points]], the effect is that these [[Small improvements compound over time]] both as a value-producer and a value-enabler.

[[Agile is a perspective not a process]], but when applied simply as a mechanistic process can lead to [[Agile in Name Only]]. [[Agile is not the goal]], it's more correct to say that [[Agility is a business capability]] that can better enable desired outcomes.

[[Real Options]] is a conceptual framework for managing an agile product backlog.

Agile places great focus on the definition of done - my opinion is that [[You're done when a need is met]]

[[User Stories]]  are a user-oriented representation of work to be done, captured in a lightweight manner (as opposed to formal functional specification documentation)