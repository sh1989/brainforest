* [[OOP needs a procedural boundary]] to separate a domain model from the side-effects
* [[Tell don't ask]] is a form of object-orientation where objects issue commands to perform a behaviour, rather than querying
* [[Indirection without abstraction is waste]] is an antipattern that occurs when objects introduce overhead through unnecessary layers of indirection
* [[Derive Booleans from state]] is an example of how using the wrong level of abstraction can introduce additional complexity