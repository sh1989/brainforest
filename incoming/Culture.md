The techniques of [[Coaching]] can help a group to change its culture

* Setting up  a culture of [[Autonomy requires accountability]]
* Avoiding a monoculture is important, to do this [[Optimise for culture add, not culture fit]]
* Values are useful for contextualising the culture
	* [[Values are not nouns]] if they are to be effective signposts
	* [[Be aligned to values]] to guide behaviour
	* [[Culture is values plus behaviour]] - practice what's preached

The concept of [[Psychological Safety]] represents a healthy working environment, as opposed to 'traditional' [[Command and Control working environments]]