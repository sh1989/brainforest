[[Culture]]

[[Culture is values plus behaviour]]

Nouns aren't sufficient because values are what we want people to do. Instead of "honesty", it's "tell the truth".

Values are not aspirational - rather than saying this is what we want to be, the values are who you are when you are at your natural best. You may not be operating always at your natural best. The question is how can you promote and live those values.