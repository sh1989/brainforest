Is the role of QA to:
* Find bugs? (By 'breaking' software)
* Prevent bugs?

In a traditional [[Waterfall Software Management]] structure, QA is considered an independent, separate validation phase which is performed after development. It's primary role is to provide an independent assessment of 'quality', from specialist [[I-Shape]] professionals.

Their feedback however, encourages [[rework]] and can constitute to being a [[bottleneck]] to Continuous Delivery. Being a [[Gated Phase]] near the end of the process, QA departments frequently get blamed for 'holding up' delivery, especially because rework compounds at later stages - fixing bugs late is expensive

The inverse of this is to see [[QA is a Continuous Process]]