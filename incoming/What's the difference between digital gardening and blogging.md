The primary sort mechanism in a blog is reverse-chronological, making it a stream-oriented presentation of information. Considering [[Streams prioritise recency over relevancy]], blog content risks losing relevancy as time passes and furthermore, risk losing out on traffic as their position in the sort order decreases.

Unless a blog post is kept evergreen through frequent revision, as per the concept of digital gardening, then the content itself may become stale. Note if the content is deliberately written to capture a snapshot in time, this can be desirable.

I used to use public-facing blog articles as a way to do my thinking, because [[Writing is thinking]], but this meant that I had to broadly have something workable in my head initially, because the intention was to publish and publicize. Using a digital garden as an [[Integrated Thinking Environment]] is about setting up a structure and location in which this can be done without the pressure of producing something concrete. Although the evergreen notes end up being public ([[Why do I make my notes public]]), what differs is ultimately the audience. [[These notes are for me]] whereas the audience of a blog is more outward-focused.

Combining these two concepts together - the importance of time in a blog, and the intended audience, I propose a working model for knowing when to blog versus when to tend in the garden:
* Externalizing thoughts as a snapshot in time
* Providing information that is tightly-coupled to a moment in time
* Making a deliberate decision to not invest in maintaining a piece of evergreen content.

The blog is not necessarily a bad medium, but blogging by default might be.