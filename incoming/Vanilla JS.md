"Vanilla JS" is an umbrella term describing web development that remains deliberately lightweight in nature, utilising the browser's native capabilities, without additional overhead of a framework.

Although commonly a reaction to the conceived high churn of JavaScript frameworks, it is useful to understand the growing capabilities of browsers to be able to effectively question whether a particular dependency is actually needed.

Many JavaScript UI frameworks recognise that [[Web content is componentised]] and developers want to build re-usable, composeable and modular components. Natively this is now provided through the use of [[Web Components]].

Other capabilities:
* DOM querying
* DOM events
* [[HTML Data Attributes]] for storing and accessing data on DOM elements
* ES6 String Literals for basic templating