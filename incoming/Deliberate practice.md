Deliberate practice is purposeful - it's done specifically to improve performance.
It is also focused - requiring active attention rather than passive effort such as repetition.

The basic algorithm is to:
1. Decompose the process
2. Identify areas to improve
3. Experiment in each section
4. Integrate the learning

A successful environment for deliberate practice is one where [[Experiments must be safe to fail]] and where feedback is plentiful, as [[Fast feedback improves quality]]. [[Minimise distractions to focus attention]] 