Making a commitment obliges you to do something, normally in a certain way, or by a certain time. At this point, the decision is no longer optional.

Commitments may be reversible, but this typically comes at a large cost.