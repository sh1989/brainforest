They are guesses!

Because:
* What we expect and what actually happens are different
* Higher estimates may encapsulate multivariate concerns, such as uncertainty
* Estimates rarely reflect reality in terms of delays or interruptions

Estimates can confuse accuracy and precision - businesses want accuracy but we can risk giving them (false) precision. Consider that an "about 6 months" project taking a month more isn't too bad. But if adding the estimates takes 4.6 months and it takes 7, that's considerably worse.

Due to an agile team [[Learn along the way]] then highly-precise estimates are wasteful, for anything that's not done.

Estimation can have a fractal approach to it as work is decomposed.