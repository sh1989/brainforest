People have a tendency to *make* a decision, even if it's the wrong decision, so that the uncertainty can become certain. This is harmful decision-making and actually, one should [[Never commit early unless you know why]]

Until we know what is the right decision, the optimal decision is *not to make a decision*. That means the conditions under which a decision should be made need to be outlined.