Remote working practices were common prior to the [[Covid-19 Pandemic]], however with the forced remote working created by lockdowns worldwide, many reluctant or resistant organisations had to make the adjustment. The change in working practices have led to a larger acceptance, and market demand for, remote working. Even as returning to shared spaces becomes a possibility, a desire for 'flexible' or 'blended' working is now considered normal, making it unlikely that knowledge workers will return full-time to the office en-masse.

Remote working has benefits and drawbacks:
* Office space is expensive, not having a need to provide all staff with a permanent desk can be seen as a cost-reducing move.
* Remote work geographically extends the hiring pool
* Avoiding the need to commute means that employers can use that time for more meaningful activities, such as family or leisure.
* Lower expenses sunk on work, that can be used to improve lifestyle in other areas
* Employees can choose where to work from (it doesn't have to be at home! It could be a cafe, a library, the beach...)
* [[Remote working can be isolating and lonely]], especially for those who live alone or will continue to shield for health reasons (without government support).

Even flexible/blended organisations will have to adapt to new practices and behaviours, [[Culture is values plus behaviour|forming a new culture with new norms]], which will be a disruptive activity. Poor management practices will be particularly noticeable during this transition, as proxy measures for performance (such as presence in an office, or who talks the most, who's friendly) will become ineffective.

Organisations should accept that even with a blended model, they are still a remote working organisation. It is more likely than not that most meetings, unless formally mandated to be fully in-person, will have a remote worker joining virtually. This changes the dynamic of how to have meetings and how to work productively. Previous personal experience of extensive remote working has often felt like virtual meeting attendants are an afterthought, whereas actually [[Remote workers are not second-class citizens]].

When we [[Give people the freedom to choose where they work]] (promoting a cultural norm of trust), it is possible to better distinguish the types of knowledge work and allow for self-organisation.