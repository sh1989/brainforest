---
alias: opex
---

Regular expenditure incurred for running day-to-day operations.

* Rent / utilities
* Salaries
* Overhead costs
* Cost of goods sold
* R&D