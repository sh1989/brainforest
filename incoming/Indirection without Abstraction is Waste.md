Often a side-effect of the desire for fast feedback, because [[Fast feedback improves quality]] - but the mechanism by which we mock creates layers of indirection. This is so that we can utilise techniques like Single Responsibility, Interface Segregation and Dependency Inversion.

These layers of indirection, if they're not adding some domain-value through higher-order abstractions, are just introducing waste. It increases the cognitive overhead to understand the code and reason about its changes. It slows down feedback because of a tendency to overmock these relationships. As [[Overmocking creates alternative facts]] it is possible for test cases going through many layers of indirection to run through fictitious code paths that aren't representative of the production-like behaviour.

Fix by [[Test behaviour not implementation]]

## References
[[Test-Induced Design Damage]]
[[Why Getter and Setter Methods are Evil]]