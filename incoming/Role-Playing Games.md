Most of the time I run (DM/GM) RPGs rather than play them.

* [[Let go of high standards to generate better ideas]]
* [[Transition between combat turns using narrative to maintain immersion]]