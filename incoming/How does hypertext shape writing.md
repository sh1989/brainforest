This [[Questions|question]] queries how the principle of [[The medium shapes the message]] applies to content written for the web in [[Hypertext]].

## Hyperlinks
In order to answer this, I looked at [[The Rhetoric of the Hyperlink]], which reflects on how we have the tendency to look at emerging technology through the lens of our current capabilities. For the practice of hyperlinking this emerges when the hyperlink is used as a means to cite a reference. There's nothing inherently wrong with this, it is a very common use case. Although, when considering the *style* of the hyperlink, there is a user-friendliness and appeal to an implicit, declarative, inline link compared to an explicit and imperative form.

That appeal of a hyperlink is fundamental to the web as a platform. [[Hyperlinks are the fundamental unit of Hypertext]], being the connective tissue between documents. This represents an inversion of control, from the writer to the reader, who is free to explore hyperlinks as they see fit. Good writing in this medium recognises that [[Hypertext embraces nonlinear exploration]] and structures content appropriately [[Hypertext and Writing]] advises that too many links, or poorly presented links can lead to a disorientating effect known as the [[Hypertext Navigation Problem]]. The writer can minimise this by taking an editorial stance on when to hyperlink. Consider linking as a value add enrichment and not to provide a distraction. [[Hyperlinks invite curiosity by blending form and content]], providing an opportunity for further exploration.

The essay [[Hypertext Gardens - Delightful Vistas]] is an early-days musing on writing for this medium, differentiating a garden and wilderness, and what properties a garden has which makes for a pleasant and welcoming experience. The principles of garden design apply to the construction of hypertext:
* There needs to be enough structure to communicate that it is not chaotic, but enough variance to maintain attention
* Architect pathways to guide visitors along a curated experience
* Provide intersections to allow for deviance and safe exploration, which also has the effect of increasing the dimensionality of the space and allows for serendipity.

## Transclusion
