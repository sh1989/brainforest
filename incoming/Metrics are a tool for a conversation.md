Because [[We can only be certain about now]], metrics can provide further clarity on the current situation, but these cannot be meaningfully projected into the future without acknowledging that there is uncertainty about the future

Story points only tell you how hard something is, they don't tell you when