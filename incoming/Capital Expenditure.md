---
alias: capex
---

Significant, long-term expenditure. For example, on goods or services used to benefit/improve future performance or capability. Fixed assets are a good example. They can depreciate over time to spread the cost over its life.

[[Software Development]] projects are typically considered capex