In contrast to physical workers, whose output is in the production of physical material, knowledge workers are in the domain of information.

The outputs of knowledge work can still be measured - for instance in [[Software Development]] through the value realised by an end user - with a key skill for a knowledge worker being the *management* and *application* of information to a particular problem.

Knowledge work can be conducted through solitary activity, but benefits can be realised through collaborative efforts.

Managing knowledge workers requires different techniques. [[Taylorism constrains knowledge workers]] so managers should look more towards [[Culture]] by [[Creating enabling environments for knowledge work]].

At the individual level, a knowledge worker should develop systems for effective knowledge management (my attempt of this is in [[Curating a Digital Garden]]) and also developing productivity structures to facilitate a state of flow, such as:
* Maximising the effectiveness of meetings: [[Set meeting agendas in advance]] and [[Always take meeting notes]]
* Minimising distractions
*  [[Buffer meetings with breathing space]]
* Refining a [[Digital Toolbelt]]

Creating an environment for knowledge work should be a strong desire of any organisation in this space, deeply embedded into the company [[Culture]]. For example, [[Interrupt Knowledge Workers as a last resort]] 