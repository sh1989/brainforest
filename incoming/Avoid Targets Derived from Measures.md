---
alias: "Goodhart's Law"
---

> When a measure becomes a target, it ceases to be a good measure.

In feedback systems, it becomes the case that measures are gamified when they are encoded as targets.

Consider [[Measures as vectors]] rather than absolute values, where appropriate, to avoid this but still allow for measures to be used in analysis.