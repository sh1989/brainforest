[[A commitment is a promise]] - an obligation to do something.

This relates to the lean principle "Defer commitments to the last responsible moment".

Until the expiry point of an option there is an opportunity to gather more information and explore the problem space. This can take place through experimentation and fast feedback - such as prototyping, spiking, user research. [[Gathering information reveals more options]] that can then ultimately inform the eventual decision.

The best point to make a commitment is at the point before expiry, when you have the most information. However, as part of managing complexity, it is often prudent to make quick or early decisions. It is important to understand *why* the decision is being made early, for instance:
* There is low value to the decision
* There is low risk
* There is high risk but an acceptable, rather than optimal, outcome sufficiently mitigates the risk
* The commitment can be reversed easily

Energy should be focused on high-value options because [[Not everything should be considered an option]].