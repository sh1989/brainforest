A good analogy for understanding "how much" should be in the User Story card.

Consider a user story card as a vacation photo - not all of the detail is present, but by looking at the photo it's possible to recall the details. It acts as a [[Mnemonic Device]].

Source: [[Jeff Patton]]