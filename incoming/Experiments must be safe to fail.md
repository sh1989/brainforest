Safe to fail experiments have a _direction_ rather than a destination as this enables parallel experimentation. The most effective experiments are performed at the edges of a system, as the centre is harder to change.

An experiment that is safe to fail is small and self-contained. This limits the negative effects of the experiment (intentional or unintentional) but also provides a tight feedback loop that can be incorporated quickly into future rounds of experimentation - as [[Fast feedback improves quality]] this can manifest as stepwise evolution towards a lower-complexity state ("nudging").

