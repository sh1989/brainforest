Git uses pointers and alters the working file tree to switch branches quickly and effectively, unlike older tools such as Subversion that treat branches as separate directories. This makes branching a quick and easy strategy, promoting the following behaviours:

* Creating long-lived branches relating to releases or stages within a wider Git-based workflow such as Gitflow
* Creating medium-length feature branches
* Creating short-lived branches as part of a [[Trunk-Based Development]] workflow
* Creating branches as a means of experimentation

Although branching is a cheap and common activity in Git, keeping code on a branch for a long period of time comes with significant maintenance costs, in the rework required to keep the changes up-to-date and in the eventual resolution of merge conflicts. [[Continuous integration optimises team flow]] by the regular practice of integrating code to a common mainline branch. Models which hinder [[Continuous Integration]], such as the Pull Request model, will slow down teams ([[Pull Requests can hinder Continuous Integration]]).

Therefore, [[Keeping branches short lived encourages Continuous Integration]] is a sound tactic for instances where a developer cannot directly commit to a mainline branch. This is typical in many large organisations who use Pull Requests as a form of [[Quality Control]].

A common misconception with branching is tightly coupling a branch to a full implementation of a feature. [[A Pull Request does not have to be a fully-implemented feature]]. However, iterative development on work-in-progress features can be conducted with regular integration through mechanisms such as feature toggling [[Feature toggles enable small increments]].