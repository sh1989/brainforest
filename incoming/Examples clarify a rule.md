In [[BDD} and [[Example Mapping for discovering unknown unknowns|Example Mapping]], the purpose of an example is to clarify some rule. [[Rules are requirements]] and therefore examples enrich the understanding of requirements.

Examples are effective when they [[Use representative data from the domain]]