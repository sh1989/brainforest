---
alias: My Digital Toolbelt
---

Batman's utility belt contains a plethora of tools for various scenarios that he regularly encounters. It is common for tradesworkers to have their own utility belts. But, this applies to [[Knowledge Working]] too. This is my personal Utility Belt.

## Programming
* Visual Studio Code is now my go-to development environment for most languages.
* IntelliJ Community Edition is my preferred development environment when working in the JVM stack.
* Postman
* NodeJS
* Docker
* The [Fira Code](https://github.com/tonsky/FiraCode) font family

### CLI utilities
#### Windows
* The new Windows Terminal
	* with Fira Code Retina
* Git Bash
	* with [Git Bash Windows Powerline](https://github.com/diesire/git_bash_windows_powerline) theme

#### Mac
* iTerm2
* Oh My ZSH with the `agnoster` theme

## Productivity
* Obsidian
* Instapaper
* Feedly