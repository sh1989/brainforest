The Technical Debt metaphor is a [[Mental Model]] describing the accumulative effect of clutter in a system, normally intended as a warning that trade-offs exist between short-term and long-term goals.

Clutter, or cruft, forms in a system by making deliberate or unintended now/later choices in the name of expediency - such as leaving corner cases unimplemented or in an error state, prioritising the delivery of a fix over writing a successful passing test, shipping a sub-optimal code structure now and deciding to fix it later.

The overall result is instability through unpredictability - in the ability to change code without incurring negative side-effects, or being able to effectively reason about those consequences. Development and delivery slows down as maintenance costs increase.

Technical Debt is an instance where [[A poor metaphor dilutes the mental model]], because debt is also a positive enabling factor.