Representing a business domain as a web of objects (in the [[Object-Oriented Programming]] style) works to a point - which is where those objects have to consider side-effects - including network, I/O, interaction with a database, and so forth.

It is normally at this point that the Object-Orientation style breaks down into something more procedurally-based, depending on exactly how the interaction with the side-effect component is required.

However, Object-Oriented principles can still be used to isolate this procedural basis, such as encapsulation.