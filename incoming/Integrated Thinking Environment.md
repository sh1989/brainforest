In the sense that an Integrated Development Environment does not do the [[Software Development]] for the user, an [[Integrated Thinking Environment facilitates thinking]] by providing the space and tools necessary, so that [[Digital gardens promote knowledge accumulation]]:

* A space for writing, as [[Writing is thinking]]
* A space for exploration of [[Questions]]
* Support for associative links ([[Link evergreen notes by contextual association]]) and their respective [[Backlinks]]
* Automated reasoning about the topology:
	* The detection of [[Implicit backlinks identify unrealised associations]]
	* Visualisation of the topology [[The graph of notes identifies unrealised associations]]