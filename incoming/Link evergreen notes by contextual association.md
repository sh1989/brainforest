In order to [[Allow ideas to emerge]], a simple presentation of facts is not sufficient. They require context and a narrative structure. This can come from:

* Asking a question - and using the digital garden as an [[Integrated Thinking Environment]]
* Consuming new literature and synthesizing it into the digital garden through the [[Record - Elaborate - Integrate]] technique
* Observing the emergent topology ([[Prefer organic structure to hierarchical structure]])

Overall, the desired effect is that [[Digital gardens promote knowledge accumulation]]