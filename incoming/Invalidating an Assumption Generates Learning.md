The idea for what to build next is a guess. It can be an educated guess, based on market research, competitor analysis, customer feedback and so on. In essence, [[Future work is an option]], whose value has some quantity (within a cone of uncertainty), but the means to extract that value is unknown.

Should the team choose to exercise this option, they can then [[Experiment towards a goal]]. As part of experimentation, identifying assumptions and seeking to validate - or invalidate - provides better clarification of user needs and aligning future product direction to those needs. We know that [[Fast feedback improves quality]].

A negative result is as useful as a positive result by providing:
* steering towards future experiments
* a risk-reducing process of discovery
* a cost-reducing opportunity, through minimizing wastage from building the wrong thing