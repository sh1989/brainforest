[[Continuous Integration]] is often misunderstood as being a build server, but it's actually the practice. The main tenet is that:

> Everyone commits to the mainline every day

As [[Branches are cheap in Git]], spinning up a branch for short-lived activity is an easy practice to do. The mainstream Git-based tooling utilises this to create the Feature Branch/Pull Request model.

However, Pull Requests can encourage developers to hold the work back until they've done everything. This essentially ends up creating long-lived branches, which is contrary to lean understanding:
* [[Fast feedback improves quality]]
* [[Small improvements compound over time]]

Furthermore, consider that [[Pull Requests can be psychologically unsafe]] in [[Command and Control working environments]]