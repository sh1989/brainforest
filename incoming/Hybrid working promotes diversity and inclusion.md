Allows employees to choose a working environment best-suited to their needs. Flexible hours accommodates other responsibilities such as parenting or caring.

[[Inclusivity benefits everyone]]