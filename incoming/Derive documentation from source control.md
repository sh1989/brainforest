A challenge in enterprise organisations with separation between developers is the emergence of shadow documentation, that doesn't stay in sync with the current state of play. Generally speaking this can be due a lack of access of tooling, or deeply-embedded information management practices that don't translate well as systems evolve. This is one manifestation of "[[A shared document is not shared understanding]]", in that the shared documentation may not even be reliably shared, or deviate.

It is more sustainable to promote the idea of [[Living documentation]]. One way to do so is to use source control tooling as the 'golden source' of information; where the documentation can be change-controlled (thus preserving history), but also utilise the power of [[Continuous Integration]] tooling to generate documentation based on the current state of the system.

This pattern is best followed when practising [[BDD]], where business scenarios are expressed in Gherkin and can be evaluated as tests to determine which scenarios have been implemented and are functional.

The concept can be extended further, to:
* Surfacing information to the business - such as analytics tracking strings, form field validation, in a form that they understand
* Security and supply-chain dependencies
* Test data (often needed for governance processes)