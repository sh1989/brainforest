Applying the scientific management approach of Frederick Winslow Taylor to improve industrial processes.

* Physical work produced manually
* Repetitive and knowable
* Quotas
* Top-down planning vs execution
* Specialized task allocation
* Performance measurement

> The work of every workman is fully planned out by the management at least one day in advance, and each man receives in most cases complete written instructions, describing in detail the task which he is to accomplish, as well as the means to be used in doing the work. This task specifies not only what is to be done but how it is to be done and the exact time allowed for doing it.

This approach may increase productivity but has side-effects: [[Taylorism constrains knowledge workers]] and has a Us Vs Them mindset.

Out of this practice came [[Project Managers]] and the [[Gantt Chart]]

With increased automation of specialised tasks came the rise of [[Fordism]] and the production lines