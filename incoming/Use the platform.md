---
alias: Lean on the Compiler
---

Also described by [[Martin Fowler]] as "Leaning on the Compiler".

Use the capabilities of the platform to their fullest extent, rather than reimplementing the same functionality on top and introducing both technical and cognitive overhead. Don't do more work than necessary!

Some good examples of when this is appropriate:

[[Design objects to make invalid state impossible]] - rather than litter the code with null/undefined checks common with [[Primitive Obsession]]

[[Linters prevent arguments over styling]] so that developers can focus on more interesting discussions at review time