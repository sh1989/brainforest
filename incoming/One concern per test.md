There's a debate over how many assertions a test should make, with a focus being on one - but this is missing the point.

A test should represent one behaviour, one concern. The entity under test may have several 'exit points', such as returning a value, calling another method, performing a third-party side-effect. Each of these exit points represent different concerns, that can fail independently, for different reasons.

So, we may need to test a single logical concern with multiple `assert` calls, but this is fine!