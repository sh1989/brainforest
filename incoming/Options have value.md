This may be monetary (it is in the financial sense), but it doesn't have to be.

[[Exercise an option to realise its value]]

 In times of uncertainty there's also value in holding the option, because [[An Option is freedom of choice]]