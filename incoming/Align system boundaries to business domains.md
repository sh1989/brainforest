Domains in [[Domain-Driven Design]] represent a [[Bounded Context]] - an area of responsibility in which there is a common understanding and representation of a process ([[Use the language of the domain]])

The domain represents a natural good fit for defining a system boundary, in that it segregates responsibility by a business function.

Often, a business process will intersect with several domains over its lifecycle. Each domain may use the same terms for different purposes. Although the same business terminology appears across these domains, each domain may have its own specialisms applied. For instance, the representation of a Customer will differ in the purchasing and fulfillment departments of an online store.