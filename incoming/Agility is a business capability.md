> To spin on the dime for a cost of a dime
   (Craig Larman)

Reflects on [[culture]]: behaviour, principles, values, norms and practices; rather than processes and tools.