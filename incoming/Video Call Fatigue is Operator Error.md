During the [[Covid-19 Pandemic]] all interactions had to be virtual, and by default were most likely replaced by video calls that tended to be longer than a phone call or a face-to-face conversation would have been.

Although video call, as a modality, is one of the more engaging forms of meeting, being on camera for extended durations of time creates fatigue.

Another reasons for this fatigue is the distracting effect of seeing our own faces on screen. Hyper-awareness of our own behaviours dragged focus from the other participants.

The gallery view of participants staring directly ahead adds to a fatigued effect of many people staring at *you*.

Solutions and mitigations:
* [[Primarily talk to the camera lens]]
* [[Consider alternative communications before booking a meeting]]
* [[Give audio only breaks when not actively participating]]