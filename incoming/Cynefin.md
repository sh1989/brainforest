## Clear
`Sense - Categorize - Respond`
Straightforward and predictable - cause and effect is clear and there are known knowns. Where best practice can be used.

Sense: I'm in the UK
Categorise: Drives on the left
Respond: By driving on the left

## Complicated
`Sense - Analyze - Respond`
**Good for [[Lean]]**
The space is knowable (even the failure patterns), known-unknowns exist, but implementation requires expertise as the relationship between cause and effect requires analysis. Good practice exists but not best practice, because based upon the analysis the appropriate practice response is applied. The space is not trivial and so there is room for process improvement.

Example: Onboarding employees -done many times before, but requires expertise.

## Complex
`Probe - Sense - Respond`
**Good for [[Agile]]**
Unique product development in a space of unknown-unknowns and emergent effects (cause and effect can only be analysed retrospectively). Consequently practice is emergent as best and good practices do not exist.

Perform probes (safe-to-fail experiments) to minimise time to learning and be able to respond to maximise value by amplifying or dampening.

Example: Software development

## Chaotic
`Act - Sense - Respond`


Act first, rapid action is more important than knowledge, so that order can be restored. Where does the stability lie and how can we respond to turn chaos into complexity? Novel Practice emerges here that can become good or best practice elsewhere.

Example: The [[Covid-19 Pandemic]]

---

There is also a state of **Confusion** in which you don't know which domain you're operating in.