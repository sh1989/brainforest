The [[Docker]] Daemon runs as a client-server architecture, where the CLI acts as the client. In order to build Docker images (which requires creating namespaces, etc) the daemon needs to run as root. This creates a [[security]] issue because the container requires a socket to the daemon running on the host. The risk is that a flaw in the container could allow 'breaking out' and running on the underlying node ful full access, as root.

There are rootless builds available such as Google's Kaniko, JIB, Podman+Buildah

Source: Container Security, Liz Rice