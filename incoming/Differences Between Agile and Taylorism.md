Multidisciplinary teams working collaboratively towards a clear outcome, over direct supervisor direction.
 * Although when the latter is still present then it leads to [[Agile in Name Only]] and when imposed through a standard implementation across an organisation, promotes the [[Agile Industrial Complex]]

Focus on value over productivity - [[Outcomes over output]]