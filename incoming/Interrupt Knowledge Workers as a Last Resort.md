The biggest risk to productivity in [[Knowledge Working]] is context switching. Furthermore, too much context switching causes fatigue, risking burnout. It is not possible to switch from one task to another without some form of downtime and loss of efficiency.

There are ways to mitigate the effect of context switching:
* [[Always take meeting notes]] so that previous conversations and actions can be recalled.
* [[Buffer meetings with breathing space]] to factor in the downtime, allocate space for reflecting and re-calibration and prevent further interruptions.

However, interruptions to knowledge workers should be considered a last resort, based on both necessity and urgency. This is an aggressive form of prioritisation but very often the cost of context switching is not factored in to a decision to perform synchronous working, rather it's a negative externality.