- Time
- Cost
- Scope

These are concerns that are usually, in some way or other, contractually committed to. Changing the level of commitment on any of these has an overall impact on the quality of delivery:
* A fixed deadline
* A fixed scope
* A fixed cost (can't add more people as this will affect P/L and may not even help)
* A fixed point of co-ordination for delivery