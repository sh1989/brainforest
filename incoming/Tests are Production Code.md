Treat unit tests like they are production code - in terms of their quality, standards, and maintenance.

Unit tests provide the path to production in the form of a quality gate - therefore if tests hinder getting changes to production, through being ridden with [[Technical Debt]], or by not clearly [[Tests demonstrate intent|communicating the intent of the code]], then the ability of the developer to deliver to production is compromised ([[Speed is a byproduct of quality]])

By [[Listen to feedback from tests|listening to tests]], a developer can identify improvements that can be made to test and application code which will improve the overall quality.