
## Perspectives
[[A User Story is a placeholder for a conversation]]

What is [[The Intent of a User Story]]?
  * [[User stories are told, not written]]
  * [[User Stories Are Options, Not Futures]]

## Mechanics
There are three aspects: [[Card, Conversation, Confirmation]]

## Right-Sizing
* [[Estimates aren't quotes]]