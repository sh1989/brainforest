[[Markdown]] has a number of appealing advantages for developers, used to working close to the wire in plain formats, that can constrain thinking about its limitations, promoting augmentation and customisation over more radical, but suitable, data representations. This is akin to a [[Local Maxima]]

Developers have enough control in simple cases. Although Markdown intentionally does not completely represent HTML, it has the option of falling back to HTML - a deliberate [[Leaky abstraction]]!

As a plain-text format it can be easily stored in source control, acting generally as an easily-parseable source of truth ([[Derive documentation from source control]]). Developers are used to working with content in this way, often eschewing [[WYSIWYG]]-style editing tools that risk [[Vendor Lock-In]].

Contrary to this understanding, the Markdown space is fragmented in nature due to augmentations to provide more complex functionality (see: Github-Flavoured Markdown, Gitlab-Flavoured Markdown, Obsidian, MDX et. al). The more this happens the lesser the benefit of using Markdown is - it affects the portability of the format, necessitating more complicated rendering engines.