
## Perspectives
[[A second brain is closer to a GPU than a CPU]] in terms of its function. It is highly specialised for retrieval so that your brain can focus on thinking.