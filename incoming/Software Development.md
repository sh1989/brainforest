* [[Agile]] development is about using fast feedback to build incrementally and iteratively
* [[Object-Oriented Programming]] is a paradigm of programming oriented around objects, communicating via messages
* [[Test-Driven Development]] is a design activity that uses test scaffolding to develop just enough functionality, and to provide a safety net for refactoring
* [[Continuous Integration]] is a team co-ordination effort that allows for teams to work collaboratively and enable the delivery of value
* [[Technical Debt]] is a metaphor describing the gradual decreasing ability to develop, and decreasing rate of delivery, of quality software.