The very act of a pull request creates a bottleneck. It is a synchronous handoff to another member(s) of the team where approval to progress is mandated.

[[Pull Requests can hinder Continuous Integration]] but they also slow down the overall project through how rework is handled.
* Rework is requested before the overall change is approved, which compounds the waiting time for the feature to be merged into the mainline
* This acts as a disincentive to review the code, which then tends towards the usage of long-lived feature branches
* The team's inventory increases as there is more work in progress
* Quality is not built in, it's added after
* The longer-lived the branch, the more merge conflicts arise as the branch is continually kept up to date with mainline