Given an element that has a [[HTML Data Attributes]] defined on it: `<div data-type="info">`

It is possible to use [[CSS]] to target the element by:
* Presence of the data attribute: `div[data-type]`
* When the data attribute has a particular value: `div[data-type="info"]`

The latter allows for customisation based on a certain property being set - such as if this attribute was `info` or `warning`.  It is a nice [[Vanilla JS]] solution that could be combined with [[Web Components]] to provide similar functionality to customisation based on React properties.