This [[Questions|question]] is about understanding the benefits from [[Curating a Digital Garden]] versus the investment needed.

I would like to use a digital garden to improve on the following aspects of myself:

1. Writing technical content precisely and more often
2. Being able to better recall the details rather than just the high level concepts
3. Develop my ability connect and synthesize ideas into a system more suited for the long-term

![[What's the difference between digital gardening and blogging]]

## How will I know I will achieve this?
I don't want to measure output directly (in terms of blogs, talks etc), but I do need to think about how to evaluate those outputs against any utility gained from this system.

How can I [[Experiment towards a goal]] in a way that even [[Invalidating an assumption generates learning]]?
* Perform a time-bound experiment - on what criteria to evaluate?