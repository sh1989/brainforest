This is my term for the aspects of [[Software Development]] that affects how software is funded, how value is extracted from software, and how this all can be managed by humans.

As [[Finance affects Software Design]] we should be considering how to work in economically efficient ways, such as:
* [[Fast feedback improves quality|Fast feedback]]
* Product ownership techniques: [[Can you consider a backlog as a portfolio of options]]
* [[Capital Expenditure|capex]] vs [[Operational Expenditure|opex]] models of funding
* Team structures ([[Product over Project Operating Model]]) respecting for [[Conway's Law]]
* Capabilities ([[Evolutionary Architecture]])