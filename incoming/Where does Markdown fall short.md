Why do developers not [[question]] the suitability of [[Markdown]] in a space where [[Web content is componentised]]?

## [[Markdown]] is Good Enough, ish
![[Markdown is a local maxima]]


## Accessibility Limitations
[[Markdown]] provides a satisfactory solution for simple developer documentation, note-taking and text-based blogging served through static site generators. Being plain-text it integrates well with source-control tooling and generating diffs. This closeness-to-the-wire limits non-technical users from fully engaging in the format without additional supportive tooling. This can lead to a disparity between documentation for developers and documentation (using a Wiki tool or traditional Office-based formats) by non-technical people.

## Next Direction
Treat content as data using [[Structured Content]]
