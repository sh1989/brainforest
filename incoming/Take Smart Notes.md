Smart notes differ from a conventional note-taking form by displaying several key properties:
* They distill ideas into the author's own voice
* They are integrated into the author's own thoughts

Incoming notes can be sourced from one of two locations:
1. From the ether. Capture these as fleeting notes to be processed later.
2. From literature. [[How to take notes from literature]]

This process results in an emergence of evergreen notes - where [[An evergreen note is a unit of knowledge]]. Achieving this comes from note taking representing thoughts ([[Take thoughtful notes]]) which are then extracted and networked into a web of other ideas. In this regard, the act of [[Writing is thinking]] - creating an environment that will [[Allow ideas to emerge]] 

Use a [[Map of Content]] to structure notes and give a workspace to flesh out a topic, or idea.