It is misconceived to plan a team's activities 'to capacity'. Rather, we should be thinking about minimizing the amount of work in progress, so that we can concentrate on streamlining the delivery of that work, to meet an identified need.

Planning to capacity (maximizing resource efficiency) creates waste in the system. In Lean terms, the team has too much *inventory*. Furthermore, when all team members are working to full capacity, any deviation induces a management crisis. In the event of rework, a prioritization call is required, which compromises the future delivery of other work in progress.

This will mean in a traditional sense that people will be sitting 'idle'. However, we are working to a *just in time* manner, which increases our responsiveness to change.