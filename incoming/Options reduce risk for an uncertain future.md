Financial options are designed to provide the holder with the right, but *not the obligation*, to perform an action. The option has a cost associated with it and a time of expiration, after which the option loses its value and cannot be exercised.

These can be used to gamble on future events, but were originally intended as a means to avoid being caught out by price hikes.

Making decisions in times of uncertainty is challenging. Taking in mind [[The three types of decision outcomes]], [[People prefer to be certain and wrong over being uncertain]]