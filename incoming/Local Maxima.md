A state that is a better fit (against some [[Fitness function]]) than its neighbours, but not necessarily the best global state.

Can be an issue when developing [[AI]] algorithms such as hill-climbing or path-finding