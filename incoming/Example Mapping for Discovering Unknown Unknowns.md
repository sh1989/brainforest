Example Mapping, a form of structured conversation for a three amigos meeting, where a user story is explored through examples.

This is a form of deliberate discovery, where we [[Learn along the way]] and develop a shared understanding. Up front, some business rules (aka acceptance criteria) may be 'known', but there will be many that aren't known. This could be because:
* it hasn't been thought about
* a business rule can be subdivided into multiple business rules

The act of discovery, through asking questions and proposing examples, helps tease out both known unknowns and unknown unknowns. Ultimately, this technique helps minimise rework, reduces the likelihood of building the wrong stuff (speculative features) and reduces the cost of software development. [[Testing is clarifying misunderstandings]]

The answer to the unknowns may not necessarily be answered right away, but can be sought independently and brought back to the team.