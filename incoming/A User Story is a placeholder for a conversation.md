This is Alastair Cockburn's original definition of a User Story from the XP days. The User Story is a deliberately-lightweight format of capturing a user need. As [[Future work is an option]] then there's no guarantee that this work will ever get prioritised. Rather, it's an option to _defer_ detailed analysis of work until the point of most responsibility. That is, when it's prioritised for maximum value. Performing detailed analysis on a user story prior to this point is costly, because it may not be the next thing to do (in a sense, the option has been exercised too early) and can incur cost through rework.

The user story is a vacation photo - containing enough information to recall the context. 

Through structured conversation, the user story can be discovered. [[Example Mapping for discovering unknown unknowns]] is one technique that can be applied to perform exploration of a user need.

Note that [[Metrics are a tool for a conversation]] and not the primary purpose 