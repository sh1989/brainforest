A Polish proverb:

> nie mój cyrk, nie moje małpy

It means "this isn't my issue and I'm not getting involved."