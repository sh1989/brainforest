Read through the resource and as you jot things down, make sure to [[Take thoughtful notes]] rather than simply copy verbatim (although taking a pull quote can have utility too).

Pull out the main ideas and extract them into atomic evergreen notes