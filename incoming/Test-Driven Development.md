As [[Fast feedback improves quality]], Test-Driven Development uses the act of automated testing as a feedback mechanism to drive a design of software.

Advice for test driven development:
* [[One concern per test]], so that tests don't become difficult to maintain
* [[Test behaviour not implementation]], so that tests do not become tightly-coupled or overspecified
* [[Overmocking creates alternative facts]], a heavy reliance of mocks for internal implementation details
* [[Use the language of the domain]] to communicate intent and avoid test overspecification
* [[A unit isn't a fixed size]] on how the unit under test can vary based on behaviour.