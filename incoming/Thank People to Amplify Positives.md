Thanking people is a means to give yourself space to decide how to feel about what you're being told. Some stuff is difficult to hear and initial reactions can be negative. Instead, thank the person who shared information - very often this could have been difficult for them to say too - and work on how to build on it.

In environments of complexity [[Amplifying positives surfaces what's working]]

Source: https://lizkeogh.com/2021/06/11/5-rules-of-coaching/