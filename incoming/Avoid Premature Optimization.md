> “Premature optimization is the root of all evil.”
> 
> Donald Ervin Knuth
> [[The Art of Computer Programming, Volume 1: Fundamental Algorithms]]

In the context of [[Software Development]] this is a means of over-engineering a solution, based on assumptions, that may have a negative overall impact on the resultant quality. For instance, the assumptions may be invalid (we [[Learn along the way]]) and baking these in incurs a high cost to change later, in the form of [[Technical Debt]]

Could this be a form of [[People prefer to be certain and wrong over being uncertain]]?