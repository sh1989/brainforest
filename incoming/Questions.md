---
alias: Question, question
---
As part of using the digital garden as an [[Integrated Thinking Environment]], I will often propose a question, which I will capture as a note ([[Evergreen notes]]) . The purpose of the note is to provide an answer to the question, and act as a 'trail' through the web of interconnections provided through the process of [[Link evergreen notes by contextual association]].

Questions are answered by consulting existing concepts and additional external resources. In both cases, new associations are developed and integrated into the existing web of thought (using [[Record - Elaborate - Integrate]]). This process is why [[Digital gardens promote knowledge accumulation]].

By following the [[Backlinks]] to this note, an intrepid reader can follow the trail of thought. The question note acts as an anchor that provides the top-level context.

Working in this way gives me the opportunity to structure answering the question, and post-hoc providing a [[Garden Path]] and to [[Learn in public]]