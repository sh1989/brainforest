Don't copy notes verbatim - translate into your own words and thought. This act of deliberate processing applies your own frame of reference, allowing
* easier recall / retrieval
* draw your own inferences
* discover patterns