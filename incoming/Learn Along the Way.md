In complex, adaptive situations, it is through the process of doing that we learn what it is we need to do.

Although we have prior experience or expectations (known knowns), and things we know we need answers for (known unknowns), there's also a third category of things we don't know yet and will discover along the way - the unknown unknowns.

Big up front thinking, as a planning or management exercise, aims to reduce the unknowns and fix them before the work begins. The reasoning can be superficially compelling from a cost perspective, as issues found later are more costly to fix. Indeed, this can even be done in an [[Agile]]way, with discovery techniques such as [[Example Mapping for discovering unknown unknowns]].

However, too much up front thinking can restrict progress. Some decisions are best taken later ([[Look for new options]]).

It is for this reason that [[Estimates aren't quotes]], there will be discoveries along the way that will change our understanding, maybe requiring different decisions ([[Estimates reveal options]]) - rarely is the same thing built in the same way in a reliable manner as in a production factory. This perspective of atomising knowledge work and reducing it to its estimates leads to low-performing teams, poor morale and the perception of working in a "feature factory".

This relates the the Lean concept of "Go and See" or the "Gemba walk"