From the agile manifesto

A sustainable pace should be able to be maintained indefinitely. This means that teams should not be operating above capacity levels, or in other means which would risk burnout.

However when measuring team velocity, as [[Velocity is not a measure of productivity]], it should not be assumed that a sustainable pace is a constant velocity. Velocity will naturally ebb and flow based on the work, time of year and other commitments.