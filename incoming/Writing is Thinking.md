An approach that precludes the act of writing by thinking up front (of structure, of content) is unproductive, leading to effects such as writer's block.

Actually it's more productive to [[Learn along the way]] and utilise the act of writing as a thinking process, to [[Allow ideas to emerge]]. Writing provides an opportunity for reflection and iteration, with the side-effect of producing an artefact.

However, bear in mind that [[The medium shapes the message]]. A thought held in the head is distinct from its representation, whether in spoken word, textual or visual. For instance, Tufte argues that the hierarchical presentation 
style of PowerPoint reinforces corporate boundaries.

There is a difference between the content produced during [[Ideation]], in which the primary purpose is as a structure for the thinking process, from the artefact's eventual desired audience. Essentially, [[Evergreen notes mature into a narrative structure]] through [[Successive refinement]], becoming structured more for readability.