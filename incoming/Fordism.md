From the Henry Ford Model T factory, which enabled mass production through:
* Production / assembly lines
* Specialisation of machinery
* Standardisation of goods - machine-produced vs handmade

Also touches into the continuous improvement and lean methodology from the Toyota production lines.

* Specialised production lines
* Pull-based workflow
* Just-in-time supply chains

[[Agile]] and [[Lean]] arise out of applying these concepts for emerging product development