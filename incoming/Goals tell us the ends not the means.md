A goal answers a question formed as: "what problem are we solving", "what benefit are we creating", and "for what group of people"?

A goal is not a target, or an activity. A goal describes an outcome that we want to achieve, because it has value. The outcome can be a behavioural change, skill acquisition, empowering a new capability. The goal however does not tell us exactly _how_ we're going to achieve that.

When formulating a goal, we should ask "how will we know we have met this goal" - this is where targets or activities may come into play.

When we have an understanding of the desired outcome, the freedom to be creative and a measure of success for the outcome, we are able to [[Experiment towards a goal]]