An Option, in the financial sense, grants the holder the right, but not the obligation, to buy (call) / sell (put) at a stated price in a specific timeframe. To do so is **exercising** the option before its expiration.

These form the basis of strategies such as speculation and hedging. To assist with this there are a series of properties known as the Greeks. Because [[We can only be certain about now]], these may have practical implications when considering future work, because [[Future work is an option]]
* Delta: the rate of change between the option's price and a change in the underlying asset price (price sensitivity)
* Theta: the rate of change between the option's price and time (time decay towards expiration)

