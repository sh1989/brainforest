We have a bell curve of ideas, ranging from terrible to great.
This means that great ideas are rare.
Holding oneself to a high standard means filtering out most ideas before they can be developed.

Actually, even average ideas can be good enough, and become great if they're allowed to develop through [[Learn along the way]]