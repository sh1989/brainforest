As a gated control mechanism, it can be very easy to utilise the act of pull request in psychologically unsafe ways:
* Creating an environment of distrust
* Gatekeeping