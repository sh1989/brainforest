We feel things and sometimes we don't like that we feel a certain way. One defense is to post-hoc rationalise why we feel this way.

But that doesn't have to be the case.

https://www.skillsyouneed.com/ps/managing-emotions.html