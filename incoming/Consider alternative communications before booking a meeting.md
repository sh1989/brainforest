To reduce the fatigue of too many meetings, consider whether there are more asynchronous forms of communication that could be suitable in place of a synchronous meeting.

[[James Stanier]] calls this "Shifting right" (on the synchronous <-> asynchronous continuum)

For information sharing, a pre-recording may suffice.