The three aspects of [[User Stories]] in the practice of [[XP]]

## Card
Just enough information to remind you about the conversation - it's just a representation of the story at a point in time
[[User Story Cards as Vacation Photos]]

## Conversation
Explore - how will we know when it's done?

## Confirmation
Explore examples to illustrate when the need is met.

Confirmations *focus* the conversation, deal with them one at a time. More cards may sprout out for later, or be discarded as unnecessary.