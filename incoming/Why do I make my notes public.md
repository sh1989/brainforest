If [[These notes are for me]] then what is the benefit of publishing them? ([[Questions]])

This helps me form a habit of active production over passive consumption - having clear outputs that result from my learning, that contributes to my growth.