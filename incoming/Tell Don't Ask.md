Tell, Don't Ask is an [[Object-Oriented Programming]] style that prefers objects to be defined in terms of the functionality that they encapsulate (i.e the role that it serves within the system), rather than as storage for mutable data.

The prime mechanism for system functionality in OOP is the *message-passing between objects*, which takes the form of method calls. Systems that utilise message passing as a way to directly read or set data ("asking") become tightly-coupled, with low cohesion, because a piece of functionality becomes diffuse within the code. An observed effect is multiple layers of unnecessary indirection, and [[Indirection without abstraction is waste]]

When messages are expressed in the form of instructions ("telling")