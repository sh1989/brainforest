---
alias: "Extreme Programming"
---

XP embraces uncertainty by focusing on the ability to continuously deliver robust code, so that feedback can be acted on early and often.

It developed into a set of technical practices.

[[Mature what naturally works]] over abandoning what works naturally in favour of "maturity".

[[Why Did Agile Win Over XP]]
