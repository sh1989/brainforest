With the rise of [[Remote Working]] and [[Hybrid Working]], there is no longer the default assumption that all collaborators will be physically co-located in the same space. This changes the nature in which we work, and which we organise work, which will overall change the organisational [[Culture]]

Crucially, [[Hybrid meetings become necessary with flexible working]]. [[Remote workers are not second-class citizens]], but as [[Remote Working is Here to Stay]], the most equitable form of meeting is to [[Treat hybrid meetings as remote-first]].

[[Poor implementations of hybrid working strengthens presenteeism]] - in this sense, good [[Hybrid working promotes diversity and inclusion]] by enabling more participation.

Office spaces therefore should be seen as places where physical collaboration can be maximised, for those situations where it is most suitable. [[Consider alternative communications before booking a meeting]] and establish whether interactions can be made asynchronous, do not assume synchronous by default.