We all want good quality software. To do this, we need to create shared understanding. Why?
* It prevents rework, when we have to realign to expectations
* [[Fast feedback improves quality]]
* [[Testing is clarifying misunderstandings]], not that we've built to specification

A shared document is not the same as shared understanding. In waterfall environments, handing requirements/specification documents between business function silos does not lead to a common understanding - everyone has their own interpretation. Furthermore, this restricts the opportunity for the fast feedback and clarifying of misunderstandings early in the process. Testers must wait until something's been built, and if they find something that needs correction, costly rework is performed.

As in the Agile Manifesto:
> The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.

Because the cheapest way to correct a bug is to prevent it happening in the first place, it follows that having high-quality structured conversations (such as [[Example Mapping for discovering unknown unknowns]]) can produce this shared understanding at an appropriate point.

This may sound waterfall-y, but it's not. We still need to [[Optimise for flow, not resource capacity]] so that we can focus our efforts on being able to [[Learn along the way]].