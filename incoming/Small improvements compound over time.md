Get into the habit of tiny improvements, especially over larger-scale changes.
But, it is hard to achieve.

The data:
> 1% improvement per day for a year results in 37.78x improvement
James Clear - Atomic Habits