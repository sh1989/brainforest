A form of [[Primitive Obsession]]  in which multivariate state is represented by a series of Boolean variables.

This risks introducing implicit state into the application via exploring the possible permutations. For instance, three Boolean flags has eight possible end states. Some of these implicit states may not relate to a state in the domain - which could introduce a security vulnerability or cause undesirable behaviour.

[[Design objects to make invalid state impossible]] - use a more appropriate representation such as an enum-based state machine. Booleans can then be *derived* from the underlying state for convenience and accessibility without the risk of introducing implicit state.