Make your calendar (specifically, your free and occupied time) public - most systems will still allow for private meetings or to hide the subjects of events. Doing so confers the ability for a meeting organiser to use scheduling tools to find a suitable time to meet, but it also means the organiser is more intentional regarding participants and the effect of overall load on participants.

Some complementary techniques:
* [[Block out focus time to reduce interruptions]]
* [[Buffer meetings with breathing space]]