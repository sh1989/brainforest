Projects are funded by allocating money for specific outcomes. This aligns tightly to [[Waterfall Software Management]].

Product funding is more continuous, perhaps on a time and materials or fixed cost basis.

## Affects on [[Culture]]
* Short-term feature factory model for products, which can accrue [[Technical Debt]] as priority is on delivery not maintenance
* Poor co-ordination between projects
* 'Hero' culture that rewards fast fixes