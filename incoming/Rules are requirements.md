A rule expresses how the system ought to behave.

In [[BDD]] use the `@Rule` annotation to describe these.

When [[Example Mapping for discovering unknown unknowns|Example Mapping]], Rules may be known in advance and presented to the group, or they may be assumed or unknown, only to be discovered through the process.