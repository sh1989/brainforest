---
alias: "Gall's Law"
---

> A complex system that works is invariably found to have evolved from a simple system that worked. A complex system designed from scratch never works and cannot be patched up to make it work. You have to start over with a working simple system.
> 
> John Gall (_Systemantics: How Systems Really Work and How They Fail_)

The World Wide Web is designed as an [[Evolutionary Architecture]], in that its simple primitive is the [[Hyperlinks are the fundamental unit of Hypertext|Hyperlink]]. The act of linking provides a bottom-up emergent effect of creating a dynamic and distributed web of documents without imposing a top-down hierarchy ([[Prefer organic structure to hierarchical structure]]).

By defining simple rules and [[Allow architecture to evolve|allowing architecture to evolve]], systems have the capability to manage complexity and be [[exaptive]] to new problems and domains.