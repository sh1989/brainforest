Organisations not capable of measuring the value being delivered will use proxy measures for performance, such as visibility, known as presenteeism.

[[Effective remote working requires trust]] - low-trust environments use presenteeism for performance measurement, as those in the office are being *seen* and therefore assumed to be doing something productive. [[Remote workers are not second-class citizens]] but in such environments they are treat as so.

[[Poor implementations of hybrid working strengthens presenteeism]] by not implementing effective working practices to allow contribution on a fair level.