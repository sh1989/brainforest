In the marketplace of ideas, the [[Agile]] Manifesto won out over [[XP|Extreme Programming]] - why is this?

The Manifesto promotes a set of principles and ideas.

Principles vs practices?
Measurable vs unmeasurable?
[[Agile Industrial Complex]]

## But Has Agile Failed?
* [[Agile and the Long Crisis of Software]]
* [[15th State of Agile Report]]
* [[War is Peace, Ignorance is Strength, Freedom is Slavery, Scrum is Agile]]
* [[Scrum is a Failure - Here's Why]]
* [[Ron Jeffries Dark Scrum Articles]]
* [[Is Agile a Cult]]?
* [[Agile 2]]

## A Re-emergence of XP
[[Putting the XP in Scrum]]