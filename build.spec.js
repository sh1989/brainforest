const { deObsidianify, slugify, obsidianExtensions } = require('./build');
const dedent = require('dedent');

function generateFileStructure(files, currentFileId) {
  const nodes = files.reduce((o, { fileName, slug }) => {
    o[slug] = {
      fileName,
      outboundLinks: [],
      inboundLinks: [],
    };
    return o;
  }, {});
  return {
    nodes,
    file: {
      slug: currentFileId,
      fileName: `${nodes[currentFileId].fileName}.md`,
    },
  };
}

describe('Converting human-readable file names to machine-readable slug identifiers', () => {
  const cases = [
    ['Agile in Name Only', 'agile-in-name-only'],
    [
      `Amplifying positives surfaces what's working`,
      'amplifying-positives-surfaces-whats-working',
    ],
    ['Record - Elaborate - Integrate', 'record-elaborate-integrate'],
  ];

  it.each(cases)('from %s to %s', (from, to) => {
    expect(slugify(from)).toEqual(to);
  });
});

describe('Converting Obsidian Markdown into Standard Markdown', () => {
  describe('when an Obsidian link is detected and the target page exists', () => {
    const { file, nodes } = generateFileStructure(
      [
        { fileName: 'A Test File', slug: 'a-test-file' },
        { fileName: 'Some Other File', slug: 'some-other-file' },
      ],
      'a-test-file'
    );
    const data = 'Just take a look at [[Some Other File]] for more info';
    let result;

    beforeAll(() => {
      result = deObsidianify(file, data, nodes);
    });

    it('converts the link', () => {
      expect(result).toContain(dedent`
        Just take a look at [Some Other File](some-other-file.html) for more info
      `);
    });

    it('tracks the outgoing link', () => {
      expect(nodes['a-test-file'].outboundLinks).toEqual(['some-other-file']);
    });

    it('tracks the inbound link with backlink context', () => {
      expect(nodes['some-other-file'].inboundLinks).toEqual([
        {
          slug: 'a-test-file',
          context: 'Just take a look at Some Other File for more info',
        },
      ]);
    });
  });

  describe('when an Obsidian link is detected and the target page does not exist', () => {
    const { file, nodes } = generateFileStructure(
      [{ fileName: 'A Test File', slug: 'a-test-file' }],
      'a-test-file'
    );

    const data = 'Just take a look at [[Some Other File]] for more info';
    let result;

    beforeAll(() => {
      result = deObsidianify(file, data, nodes);
    });

    it('removes the link', () => {
      expect(result).toContain(dedent`
        Just take a look at Some Other File for more info
      `);
    });

    it('does not track the outgoing link', () => {
      expect(nodes['a-test-file'].outboundLinks).toEqual([]);
    });
  });

  describe('when there are multiple Obsidian links in a paragraph', () => {
    const { file, nodes } = generateFileStructure(
      [
        { fileName: 'A Test File', slug: 'a-test-file' },
        { fileName: 'Some Other File', slug: 'some-other-file' },
        { fileName: 'A Third File', slug: 'a-third-file' },
        { fileName: 'The Help Page', slug: 'the-help-page' },
      ],
      'a-test-file'
    );
    const data = dedent`
      Just take a look at [[Some Other File]] for more info, or alternatively there's [[A Third File]] and eventually [[A Fourth File]]. For now, consult [[The Help Page]].
    `;
    let result;

    beforeAll(() => {
      result = deObsidianify(file, data, nodes);
    });

    it('converts the links for existing pages only', () => {
      expect(result).toContain(dedent`
        Just take a look at [Some Other File](some-other-file.html) for more info, or alternatively there's [A Third File](a-third-file.html) and eventually A Fourth File
      `);
    });

    it('tracks the outgoing links', () => {
      expect(nodes['a-test-file'].outboundLinks).toEqual([
        'some-other-file',
        'a-third-file',
        'the-help-page',
      ]);
    });

    it('tracks the inbound links with the containing sentence of context', () => {
      expect(nodes['some-other-file'].inboundLinks).toEqual([
        {
          slug: 'a-test-file',
          context: `Just take a look at Some Other File for more info, or alternatively there's A Third File and eventually A Fourth File.`,
        },
      ]);
      expect(nodes['a-third-file'].inboundLinks).toEqual([
        {
          slug: 'a-test-file',
          context: `Just take a look at Some Other File for more info, or alternatively there's A Third File and eventually A Fourth File.`,
        },
      ]);
      expect(nodes['the-help-page'].inboundLinks).toEqual([
        {
          slug: 'a-test-file',
          context: `For now, consult The Help Page.`,
        },
      ]);
    });
  });

  describe('when there are Obsidian links in an unordered list', () => {
    const { file, nodes } = generateFileStructure(
      [
        { fileName: 'A Test File', slug: 'a-test-file' },
        { fileName: 'Some Other File', slug: 'some-other-file' },
        { fileName: 'A Third File', slug: 'a-third-file' },
      ],
      'a-test-file'
    );
    const data = dedent`
      * See [[Some Other File]]
      * [[A Third File]]
      * A Fourth File
    `;
    let result;

    beforeAll(() => {
      result = deObsidianify(file, data, nodes);
    });

    it('converts the links for existing pages only', () => {
      expect(result).toContain(dedent`
        * See [Some Other File](some-other-file.html)\n
        * [A Third File](a-third-file.html)\n
        * A Fourth File
      `);
    });

    it('tracks the outgoing links', () => {
      expect(nodes['a-test-file'].outboundLinks).toEqual([
        'some-other-file',
        'a-third-file',
      ]);
    });

    it('tracks the inbound links with the bulletpoint removed', () => {
      expect(nodes['some-other-file'].inboundLinks).toEqual([
        {
          slug: 'a-test-file',
          context: `See Some Other File`,
        },
      ]);
      expect(nodes['a-third-file'].inboundLinks).toEqual([
        {
          slug: 'a-test-file',
          context: `A Third File`,
        },
      ]);
    });
  });

  describe('when an Obsidian link has a secondary name part', () => {
    const { file, nodes } = generateFileStructure(
      [
        { fileName: 'A Test File', slug: 'a-test-file' },
        { fileName: 'Some Other File', slug: 'some-other-file' },
      ],
      'a-test-file'
    );
    const data =
      'Just take a look at [[Some Other File|this file here]] for more info';
    let result;

    beforeAll(() => {
      result = deObsidianify(file, data, nodes);
    });

    it('converts the link', () => {
      expect(result).toContain(dedent`
        Just take a look at [this file here](some-other-file.html) for more info
      `);
    });

    it('tracks the outgoing link', () => {
      expect(nodes['a-test-file'].outboundLinks).toEqual(['some-other-file']);
    });

    it('tracks the inbound link with backlink context', () => {
      expect(nodes['some-other-file'].inboundLinks).toEqual([
        {
          slug: 'a-test-file',
          context: 'Just take a look at this file here for more info',
        },
      ]);
    });
  });

  describe('when an Obsidian link has a transclusion prefix', () => {
    const { file, nodes } = generateFileStructure(
      [
        { fileName: 'A Test File', slug: 'a-test-file' },
        { fileName: 'Some Other File', slug: 'some-other-file' },
      ],
      'a-test-file'
    );
    const data = 'Just take a look at ![[Some Other File]] for more info';
    let result;

    beforeAll(() => {
      result = deObsidianify(file, data, nodes);
    });

    it('leaves the link untouched in deObsidianify', () => {
      expect(result).toContain(dedent`
        Just take a look at ![[Some Other File]] for more info
      `);
    });

    it('tracks the outgoing link', () => {
      expect(nodes['a-test-file'].outboundLinks).toEqual(['some-other-file']);
    });

    it('tracks the inbound link with backlink context', () => {
      expect(nodes['some-other-file'].inboundLinks).toEqual([
        {
          slug: 'a-test-file',
          context: 'Just take a look at Some Other File for more info',
        },
      ]);
    });
  });

  describe('callouts', () => {
    const extension = obsidianExtensions()[0];

    it('Converts markdown into a callout block', () => {
      const data = dedent`
        > [!info] This is a callout
        > With some content
        > plus a second line
        And something that's not inside the callout`.replaceAll('\n', '\n\n'); // Mimics showdown line ending normalisation behaviour

      const result = data.replace(
        new RegExp(extension.regex, 'g'),
        extension.replace
      );
      expect(result).toEqual(dedent`
        <div class="callout" data-callout="info">
          <div class="callout-title"><span> This is a callout</span></div>
          <div class="callout-content">
            <p>
              With some content<br>plus a second line
            </p>
          </div>
        </div>

        And something that's not inside the callout
      `);
    });
  });

  describe('transclusions', () => {
    const extension = obsidianExtensions()[1];

    it('Converts the transclusion link into an iframe', () => {
      const data = `![[A Transclusion]]`;

      const result = data.replace(
        new RegExp(extension.regex, 'g'),
        extension.replace
      );
      expect(result).toEqual(`<iframe src="./a-transclusion.html" />`);
    });
  });
});
