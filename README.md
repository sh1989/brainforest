# Brainforest

An interactive digital garden browser - will dynamically explore the smart notes, one linked note to another.

## Getting Started

- `npm i`
- `npx husky install`

## Local Development

Locally, parsed notes are stored in a gitignore'd `notes` directory and served using an `express` server:

- Copy the evergreen notes from Obsidian to the `incoming` folder
- Run `npm run clean` to clear out the notes directory. Then, `mkdir notes`.
- Run `npm run build` to parse the notes from Obsidian markdown to HTML content.
- Start the server with `npm start`
