![Sam Hogarth](shogarth.jpg =100%x*)

I'm <a href="https://samhogy.co.uk" class="external">Sam Hogarth</a>. May day job is in the world of [[Software Development]]. Every year I decorate the house for <a href="https://samhogy.co.uk/halloween" class="external">Halloween</a>, and I love to play (well, mostly run) [[Role-Playing Games]].

Although I wouldn't call myself a polymath, I've always been someone who knows a good amount about a wide range of topics. This Zettelkasten-like system, that I live in using <a href="https://obsidian.md" class="external">Obsidian</a>, is my way of storing concepts, thoughts and ideas. This note-browsing web application is bespoke, built for myself and my particular use case.
