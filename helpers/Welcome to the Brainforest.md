![brainforest](brainforest.png =100%x*)

This is my digital garden - a place where I cultivate my knowledge. It's a space to explore, to think and to learn. Consider it a "choose your own adventure" through my brain - there's no right or wrong path and there's little by way of a formal hierarchy of information. Explore whatever trail takes your fancy. Take in the lovely scents and pretty views, but don't mind the weeds.

Want to learn more about [[Curating a Digital Garden]]?

You may wish to learn more about the gardener ([[About Me]]), or what topics are currently at the [[Top of my Mind]], or even what [[Questions]] I'm thinking about. The full list of notes is at the [[Big Map of Content]].
