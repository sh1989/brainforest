const fs = require('fs');
const path = require('path');
const util = require('util');
const showdown = require('showdown');
const encoding = 'utf8';
const dedent = require('dedent');

function obsidianExtensions() {
  return [
    {
      // Callouts
      type: 'lang',
      regex: /> \[\!([a-z]*)\](.*)((\n\n> .*)*)/,
      replace: function (_, calloutType, calloutTitle, calloutContent) {
        return dedent`
        <div class="callout" data-callout="${calloutType}">
          <div class="callout-title"><span>${calloutTitle}</span></div>
          <div class="callout-content">
            <p>
              ${calloutContent
                .split('\n\n')
                .filter((s) => !!s)
                .map((s) => s.replace('> ', ''))
                .join('<br>')}
            </p>
          </div>
        </div>`;
      },
    },
    {
      // Transclusions
      type: 'lang',
      regex: /\!\[\[(.*)\]\]/,
      replace: function (_, title) {
        return `<iframe src="./${slugify(title)}.html" />`;
      },
    },
  ];
}

const outputDir = process.env.NODE_ENV === 'production' ? 'public' : 'notes';
const file = {
  readdir: util.promisify(fs.readdir),
  readFile: util.promisify(fs.readFile),
};
const incomingFolder = 'incoming';
const helperFolder = 'helpers';

async function main() {
  const stats = {
    lastPublished: new Date(),
  };

  const converter = new showdown.Converter({
    noHeaderId: true,
    parseImgDimensions: true,
    strikethrough: true,
    extensions: [obsidianExtensions],
  });

  const files = await getFilesToRead();
  await read(files, stats);

  stats.markdownFiles.forEach((file) => {
    applyModifications(file, stats);
    return convertToHtml(file, converter);
  });

  report(stats);
}

async function getFilesToRead() {
  const incomingFiles = await file.readdir(incomingFolder);
  const helperFiles = await file.readdir(helperFolder);

  return incomingFiles
    .map((fileName) => path.join(incomingFolder, fileName))
    .concat(helperFiles.map((fileName) => path.join(helperFolder, fileName)));
}

async function read(files, stats) {
  const nodes = files.reduce((agg, fileNameWithPath) => {
    const { fileName, slug } = fileDetails(fileNameWithPath);
    agg[slug] = {
      fileName,
      outboundLinks: [],
      inboundLinks: [],
    };
    return agg;
  }, {});

  const markdownFiles = await Promise.all(
    files.map((fileNameWithPath) =>
      file.readFile(fileNameWithPath, encoding).then((data) => {
        const { fileName, slug } = fileDetails(fileNameWithPath);
        return {
          slug,
          data: deObsidianify({ fileName, slug }, data, nodes),
        };
      })
    )
  );

  stats.nodes = nodes;
  stats.links = Object.keys(nodes).reduce(
    (acc, n) => (acc += nodes[n].outboundLinks.length),
    0
  );
  stats.markdownFiles = markdownFiles;
}

function fileDetails(fileNameWithPath) {
  const fileName = fileNameWithPath
    .replace(`${incomingFolder}${path.sep}`, '')
    .replace(`${helperFolder}${path.sep}`, '')
    .split('.')[0];
  return {
    fileName,
    slug: slugify(fileName),
  };
}

function slugify(s) {
  return s
    .toLowerCase()
    .replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-') // collapse dashes
    .replace(/^-+/, '') // trim - from start of text
    .replace(/-+$/, '') // trim - from end of text
    .replace(/-/g, '-');
}

function deObsidianify({ fileName, slug }, data, nodes) {
  const title = `# ${fileName.split('.')[0]}`;
  const FRONT_MATTER_REGEX = /---\n.*\n---/;
  const NEWLINE_REGEX = /[\r\n]+/g;
  const SENTENCE_WITH_OBSIDIAN_LINKS_REGEX = /[^.]*(!)?\[\[[^.?!:]*[.?!:]?/g;
  const OBSIDIAN_LINK_REGEX = /(!)?\[\[[A-Za-z,'\-| ]+\]\]/g;

  const adjusted = data
    .replace(FRONT_MATTER_REGEX, '')
    .split(NEWLINE_REGEX)
    .map((line) =>
      line.replace(SENTENCE_WITH_OBSIDIAN_LINKS_REGEX, (sentenceMatch) =>
        sentenceMatch.replace(OBSIDIAN_LINK_REGEX, (linkMatch) => {
          const theLink = linkMatch.replace(/(!)?\[\[/g, '').replace(']]', '');
          const linkParts = theLink.split('|');
          const linkedSlug = slugify(linkParts[0]);
          const linkText = linkParts.length > 1 ? linkParts[1] : linkParts[0];

          if (nodes[linkedSlug]) {
            nodes[linkedSlug].inboundLinks.push({
              slug,
              context: sentenceMatch
                .replace(/\[\[.*\|/g, '[[')
                .replace(/(!)?\[\[/g, '')
                .replace(/\]\]/g, '')
                .replace('* ', '')
                .trim(),
            });
            nodes[slug].outboundLinks.push(linkedSlug);

            if (linkMatch.match(/!\[\[/)) {
              return linkMatch;
            }
            return `[${linkText}](${linkedSlug}.html)`;
          }

          return linkText;
        })
      )
    )
    .join('\n\n');

  return `${title}\n\n${adjusted}`;
}

function applyModifications(file, stats) {
  switch (file.slug) {
    case 'welcome-to-the-brainforest':
      file.data += statsHTMLTemplate(stats);
      break;
    case 'big-map-of-content':
      file.data += mapOfContentHTMLTemplate(stats);
      break;
    default:
      file.data += backlinksHTMLTemplate(stats.nodes[file.slug], stats);
  }
}

const backlinksHTMLTemplate = ({ inboundLinks }, { nodes }) =>
  inboundLinks.length
    ? dedent`\n
    <section id="backlinks">
    <h3>Links here</h3>
    <ul>
    ${inboundLinks
      .map(
        ({ slug, context }) => `
      <li>
        <a href="./${slug}.html">
          <h4>${nodes[slug].fileName}</h4>
          <p>${context}</p>
        </a>
      </li>
    `
      )
      .join('\n')}
    </ul>
    </section>
    `
    : '';

const statsHTMLTemplate = ({ lastPublished, links, nodes }) => dedent`\n
  <h2>The Details</h2>
  <ul>
  <li>Last published: ${lastPublished.toLocaleDateString()}</li>
  <li>There are ${
    Object.keys(nodes).length
  } notes in the brainforest with ${links} links</li>
  <li>Spot anything wrong? <a class="external" href="https://gitlab.com/sh1989/brainforest/">The source code is here</a>, please file an issue</li>
  </ul>
  `;

const mapOfContentHTMLTemplate = ({ nodes }) => dedent`\n
  <ul>
  ${Object.keys(nodes)
    .map(
      (slug) =>
        `\t<li><a href="./${slug}.html">${nodes[slug].fileName}</a></li>`
    )
    .join('\n')}
  </ul>
  `;

function convertToHtml(file, converter) {
  const html = converter.makeHtml(file.data);
  const fileName = `${file.slug}.html`;
  fs.writeFileSync(path.join(outputDir, fileName), html, { encoding });
}

function report(stats) {
  console.log('Note structure:');
  console.log(stats.nodes);

  const orphans = Object.keys(stats.nodes)
    .filter(
      (n) =>
        stats.nodes[n].inboundLinks.length === 0 &&
        n !== 'welcome-to-the-brainforest'
    )
    .map((n) => `* ${stats.nodes[n].fileName}`);
  console.log(`With the following orphaned files:\n${orphans.join('\n')}`);
}

module.exports = {
  main,
  slugify,
  deObsidianify,
  obsidianExtensions,
};
