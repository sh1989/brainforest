(function () {
  const notes = document.querySelector('#notes');
  const baseTitle = document.title;
  let loadedArticles = [];

  function collapse(element) {
    element.classList.add('collapsed');
    element.scrollTo(0, 0);
  }

  function expand(element) {
    element.classList.remove('collapsed');
  }

  function toggle(element) {
    if (element.classList.contains('collapsed')) {
      expand(element);
      markLocation(element.id);
    } else {
      collapse(element);
    }
  }

  function markLocation(id) {
    if (history.state?.id !== id) {
      history.pushState({ id }, '', `#${id}`);
      setTitle(id);
    }
  }

  function setTitle(id) {
    const title = document.querySelector(`#${id} h1`).textContent;
    document.title = `${baseTitle} | ${title}`;
  }

  async function getArticleContent(url) {
    const res = await fetch(url);
    if (!res.ok) {
      throw new Error(res.status);
    }
    return await res.text();
  }

  function animate(element, className) {
    element.classList.add(className);
    element.addEventListener(
      'animationend',
      () => {
        element.classList.remove(className);
      },
      { once: true }
    );
  }

  function createArticle(id, content, initialLoad) {
    loadedArticles.push(id);
    const article = document.createElement('article');
    article.id = id;
    if (!initialLoad) {
      animate(article, 'appended');
    }
    article.innerHTML = content;
    notes.appendChild(article);
    markLocation(id);
    bindEvents(id);
    window.setTimeout(() => {
      const notesSection = document.getElementById('notes');
      notesSection.scrollTo(0, article.offsetTop - notesSection.offsetTop);
    }, 0);
  }

  async function navigateToArticle(url, initialLoad) {
    const id = url.replace('./', '').replace('.html', '');

    if (loadedArticles.indexOf(id) >= 0) {
      const existingArticle = document.getElementById(id);
      expand(existingArticle);
      markLocation(id);
      return Promise.resolve();
    }

    const content = await getArticleContent(url);
    return createArticle(id, content, initialLoad);
  }

  function collapseRightwardsItems(id) {
    const articleIndex = loadedArticles.indexOf(id);
    const toRemoveToTheRight = loadedArticles.slice(articleIndex + 1);
    loadedArticles = loadedArticles.slice(0, articleIndex + 1);
    toRemoveToTheRight.forEach((rid) => document.getElementById(rid).remove());
  }

  function bindEvents(id) {
    const article = document.getElementById(id);
    const links = article.querySelectorAll('a:not(.external)');
    links.forEach((link) => {
      link.addEventListener('click', (e) => {
        e.preventDefault();

        collapseRightwardsItems(id);

        navigateToArticle(link.getAttribute('href'), false)
          .then(() => {
            collapse(article);
          })
          .catch((_) => {});
      });
    });
    const header = article.querySelector('h1');
    header.addEventListener('click', (e) => {
      e.preventDefault();
      toggle(article);
    });
  }

  async function main() {
    window.onpopstate = function (event) {
      if (event.state?.id) {
        const id = event.state.id;

        if (loadedArticles.indexOf(id) >= 0) {
          collapseRightwardsItems(id);
          const existingArticle = document.getElementById(id);
          setTitle(id);
          expand(existingArticle);
        } else {
          const currentArticleId = history.state?.id;
          navigateToArticle(id + '.html')
            .then(() => {
              if (
                currentArticleId &&
                loadedArticles.indexOf(currentArticleId) >= 0
              ) {
                collapse(document.querySelector(`#${currentArticleId}`));
              }
            })
            .catch((_) => {});
        }
      }
    };

    const directLink = window.location.hash;
    const intro = 'welcome-to-the-brainforest';
    if (directLink && directLink !== `#${intro}`) {
      try {
        await navigateToArticle(directLink.split('#')[1] + '.html', true);
      } catch (error) {
        console.error(error);
      }
    } else {
      await navigateToArticle(`${intro}.html`, true);
    }

    document.querySelector('h1').addEventListener('click', (e) => {
      e.preventDefault();
      navigateToArticle(`${intro}.html`, false);
    });
  }

  main();
})();
